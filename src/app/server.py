from flask import Flask
from flask.ext.restful import Api

app = Flask(__name__)
app.config.from_object('app.config')

api = Api(app)

from .views import DbView, UserView, UserListView, VersionView

api.add_resource(DbView, '/db')
api.add_resource(UserListView, '/users')
api.add_resource(UserView, '/users/<int:id>')
api.add_resource(VersionView, '/version')
