from flask.ext import restful
from flask.ext.restful import fields, marshal_with

from .models import db, User
from .forms import UserCreateForm

user_fields = {
    'id': fields.Integer,
    'email': fields.String
}


class DbView(restful.Resource):
    def get(self):
        db.create_all()
        return 'OK'

    def delete(self):
        db.drop_all()
        return 'OK'

class UserView(restful.Resource):
    @marshal_with(user_fields)
    def get(self, id):
        user = User.query.filter_by(id=id).first()
        if not user:
            return '', 404
        else:
            return user

class UserListView(restful.Resource):
    @marshal_with(user_fields)
    def post(self):
        form = UserCreateForm()
        if not form.validate_on_submit():
            return form.errors, 422
 
        user = User(form.email.data)
        db.session.add(user)
        db.session.commit()
        return user

    @marshal_with(user_fields)
    def get(self):
        users = User.query.all()
        return users

class VersionView(restful.Resource):
    def get(self):
        return '0.1'
