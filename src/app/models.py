import os

from flask.ext.sqlalchemy import SQLAlchemy
from wtforms.validators import Email

from .server import app

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://realy:realy@%s/realy' % (os.getenv('DB_PORT_5432_TCP_ADDR', 'localhost'))
db = SQLAlchemy(app)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), unique=True, nullable=False, info={'validators': Email()})
 
    def __init__(self, email):
        self.email = email
 
    def __repr__(self):
        return '<User %r>' % self.email
