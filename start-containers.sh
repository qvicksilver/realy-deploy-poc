#!/bin/sh
set -e

echo 'Starting realy-db container'
docker run -d -p 5432:5432 --name realy-db -e PASSWORD=realy -e USER=realy -e SCHEMA=realy qvicksilver/realy-postgresql > /dev/null

echo -n 'Waiting for postgresql to start'
while ! docker logs realy-db 2>&1 | fgrep -q 'Below are your configured options'
do
  echo -n .
done
echo done
sleep 3

echo 'Starting realy-flask container'
docker run --privileged=true -d -p 5000:5000 --name realy-flask -v $(pwd)/src:/src --link realy-db:db qvicksilver/realy-flask > /dev/null
