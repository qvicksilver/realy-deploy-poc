#!/bin/sh

echo 'Stopping and removing realy-flask and realy-db containers'
docker stop realy-flask realy-db > /dev/null
docker rm realy-flask realy-db > /dev/null
